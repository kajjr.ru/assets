
miyuki.png
size: 1024,256
format: RGBA8888
filter: Linear,Linear
repeat: none
0
  rotate: false
  xy: 483, 19
  size: 30, 48
  orig: 30, 48
  offset: 0, 0
  index: -1
1
  rotate: false
  xy: 367, 69
  size: 146, 185
  orig: 146, 185
  offset: 0, 0
  index: -1
10
  rotate: true
  xy: 879, 180
  size: 74, 134
  orig: 74, 134
  offset: 0, 0
  index: -1
11
  rotate: true
  xy: 367, 2
  size: 65, 114
  orig: 65, 114
  offset: 0, 0
  index: -1
1113
  rotate: true
  xy: 660, 2
  size: 36, 41
  orig: 36, 41
  offset: 0, 0
  index: -1
1114
  rotate: true
  xy: 627, 2
  size: 33, 31
  orig: 33, 31
  offset: 0, 0
  index: -1
12
  rotate: false
  xy: 693, 40
  size: 34, 50
  orig: 34, 50
  offset: 0, 0
  index: -1
13
  rotate: false
  xy: 992, 130
  size: 30, 48
  orig: 30, 48
  offset: 0, 0
  index: -1
14
  rotate: false
  xy: 627, 37
  size: 28, 68
  orig: 28, 68
  offset: 0, 0
  index: -1
15
  rotate: false
  xy: 807, 122
  size: 34, 80
  orig: 34, 80
  offset: 0, 0
  index: -1
16
  rotate: true
  xy: 273, 18
  size: 28, 68
  orig: 28, 68
  offset: 0, 0
  index: -1
17
  rotate: false
  xy: 843, 130
  size: 34, 72
  orig: 34, 72
  offset: 0, 0
  index: -1
18
  rotate: true
  xy: 669, 92
  size: 110, 136
  orig: 110, 136
  offset: 0, 0
  index: -1
19
  rotate: false
  xy: 2, 9
  size: 183, 245
  orig: 183, 245
  offset: 0, 0
  index: -1
2
  rotate: false
  xy: 273, 10
  size: 57, 6
  orig: 57, 6
  offset: 0, 0
  index: -1
21
  rotate: false
  xy: 879, 127
  size: 57, 51
  orig: 57, 51
  offset: 0, 0
  index: -1
22
  rotate: false
  xy: 515, 17
  size: 110, 88
  orig: 110, 88
  offset: 0, 0
  index: -1
220
  rotate: true
  xy: 510, 13
  size: 4, 3
  orig: 4, 3
  offset: 0, 0
  index: -1
221
  rotate: true
  xy: 657, 92
  size: 13, 8
  orig: 13, 8
  offset: 0, 0
  index: -1
2223
  rotate: false
  xy: 187, 2
  size: 51, 44
  orig: 51, 44
  offset: 0, 0
  index: -1
2224
  rotate: true
  xy: 240, 2
  size: 44, 31
  orig: 44, 31
  offset: 0, 0
  index: -1
23
  rotate: false
  xy: 187, 48
  size: 178, 206
  orig: 178, 206
  offset: 0, 0
  index: -1
24
  rotate: true
  xy: 669, 204
  size: 50, 208
  orig: 50, 208
  offset: 0, 0
  index: -1
3
  rotate: false
  xy: 938, 136
  size: 52, 42
  orig: 52, 42
  offset: 0, 0
  index: -1
4
  rotate: true
  xy: 703, 2
  size: 36, 40
  orig: 36, 40
  offset: 0, 0
  index: -1
5
  rotate: false
  xy: 807, 104
  size: 29, 16
  orig: 29, 16
  offset: 0, 0
  index: -1
6
  rotate: false
  xy: 483, 2
  size: 25, 15
  orig: 25, 15
  offset: 0, 0
  index: -1
7
  rotate: false
  xy: 2, 3
  size: 12, 4
  orig: 12, 4
  offset: 0, 0
  index: -1
8
  rotate: false
  xy: 515, 107
  size: 152, 147
  orig: 152, 147
  offset: 0, 0
  index: -1
9
  rotate: false
  xy: 657, 40
  size: 34, 50
  orig: 34, 50
  offset: 0, 0
  index: -1
