
tomo.png
size: 1024,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
221
  rotate: true
  xy: 975, 458
  size: 48, 30
  orig: 48, 30
  offset: 0, 0
  index: -1
accessory_2
  rotate: true
  xy: 2, 2
  size: 212, 289
  orig: 212, 289
  offset: 0, 0
  index: -1
accessory_l
  rotate: true
  xy: 401, 133
  size: 81, 43
  orig: 81, 43
  offset: 0, 0
  index: -1
accessory_r
  rotate: true
  xy: 975, 508
  size: 74, 45
  orig: 74, 45
  offset: 0, 0
  index: -1
accessory_r3
  rotate: false
  xy: 871, 721
  size: 150, 298
  orig: 150, 298
  offset: 0, 0
  index: -1
arm_l
  rotate: true
  xy: 572, 3
  size: 86, 126
  orig: 86, 126
  offset: 0, 0
  index: -1
arm_r
  rotate: true
  xy: 700, 3
  size: 86, 126
  orig: 86, 126
  offset: 0, 0
  index: -1
armupp_l
  rotate: false
  xy: 943, 126
  size: 76, 120
  orig: 76, 120
  offset: 0, 0
  index: -1
armupp_r
  rotate: false
  xy: 943, 4
  size: 76, 120
  orig: 76, 120
  offset: 0, 0
  index: -1
brow
  rotate: true
  xy: 975, 584
  size: 135, 22
  orig: 135, 22
  offset: 0, 0
  index: -1
cheek_l
  rotate: false
  xy: 828, 25
  size: 72, 39
  orig: 72, 39
  offset: 0, 0
  index: -1
cheek_r
  rotate: false
  xy: 401, 93
  size: 62, 38
  orig: 62, 38
  offset: 0, 0
  index: -1
hair_b
  rotate: false
  xy: 2, 216
  size: 469, 803
  orig: 469, 803
  offset: 0, 0
  index: -1
hair_f
  rotate: true
  xy: 473, 461
  size: 558, 396
  orig: 558, 396
  offset: 0, 0
  index: -1
hair_l
  rotate: false
  xy: 871, 449
  size: 102, 270
  orig: 102, 270
  offset: 0, 0
  index: -1
hair_r
  rotate: false
  xy: 293, 10
  size: 106, 204
  orig: 106, 204
  offset: 0, 0
  index: -1
head
  rotate: false
  xy: 473, 91
  size: 381, 368
  orig: 381, 368
  offset: 0, 0
  index: -1
leg_l
  rotate: false
  xy: 944, 278
  size: 71, 169
  orig: 71, 169
  offset: 0, 0
  index: -1
leg_r
  rotate: true
  xy: 401, 18
  size: 71, 169
  orig: 71, 169
  offset: 0, 0
  index: -1
mouth
  rotate: false
  xy: 975, 449
  size: 19, 7
  orig: 19, 7
  offset: 0, 0
  index: -1
thigh_l
  rotate: false
  xy: 856, 66
  size: 85, 180
  orig: 85, 180
  offset: 0, 0
  index: -1
thigh_r
  rotate: false
  xy: 856, 248
  size: 86, 199
  orig: 86, 199
  offset: 0, 0
  index: -1

tomo2.png
size: 1024,512
format: RGBA8888
filter: Linear,Linear
repeat: none
1113
  rotate: false
  xy: 924, 317
  size: 89, 103
  orig: 89, 103
  offset: 0, 0
  index: -1
1114
  rotate: true
  xy: 701, 112
  size: 89, 103
  orig: 89, 103
  offset: 0, 0
  index: -1
2223
  rotate: false
  xy: 2, 2
  size: 128, 110
  orig: 128, 110
  offset: 0, 0
  index: -1
2224
  rotate: false
  xy: 701, 310
  size: 128, 110
  orig: 128, 110
  offset: 0, 0
  index: -1
accessory_l1
  rotate: false
  xy: 2, 114
  size: 180, 306
  orig: 180, 306
  offset: 0, 0
  index: -1
accessory_r1
  rotate: false
  xy: 184, 126
  size: 167, 294
  orig: 167, 294
  offset: 0, 0
  index: -1
accessory_r2
  rotate: false
  xy: 353, 131
  size: 170, 289
  orig: 170, 289
  offset: 0, 0
  index: -1
body1
  rotate: false
  xy: 525, 136
  size: 174, 284
  orig: 174, 284
  offset: 0, 0
  index: -1
eye_l
  rotate: false
  xy: 701, 203
  size: 116, 105
  orig: 116, 105
  offset: 0, 0
  index: -1
eye_r
  rotate: false
  xy: 831, 315
  size: 91, 105
  orig: 91, 105
  offset: 0, 0
  index: -1
